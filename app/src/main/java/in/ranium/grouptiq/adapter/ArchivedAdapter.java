package in.ranium.grouptiq.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.ranium.grouptiq.model.ActiveModel;
import in.ranium.grouptiq.R;

/**
 * Created by durwas on 27/04/2015.
 */
public class ArchivedAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ActiveModel> agentlist;
    private LayoutInflater inflater;

    public ArchivedAdapter(Context context, ArrayList<ActiveModel> agentlist) {
        this.context = context;
        this.agentlist = agentlist;
        inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return agentlist.size();
    }

    @Override
    public Object getItem(int position) {
        return agentlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return agentlist.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.active_list_row, null);
        }
        if (position % 2 == 1) {
            convertView.setBackgroundResource(R.drawable.list_item_green_new);
        } else {
            convertView.setBackgroundResource(R.drawable.list_item_orange_new);
        }

        final ActiveModel model = (ActiveModel) getItem(position);
        TextView agentnameTextView = (TextView) convertView.findViewById(R.id.agentnameTV);
        TextView lastloginTextView = (TextView) convertView.findViewById(R.id.lastloginTV);
        agentnameTextView.setText(model.getName());
        lastloginTextView.setText(model.getSlug());

        return convertView;
    }
}
