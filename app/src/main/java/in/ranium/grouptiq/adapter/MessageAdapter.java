package in.ranium.grouptiq.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import in.ranium.grouptiq.model.AdsModel;
import in.ranium.grouptiq.MessagedetailsActivity;
import in.ranium.grouptiq.R;

/**
 * Created by durwas on 31/12/15.
 */
public class MessageAdapter extends ArrayAdapter<AdsModel>
{
    Context context;

    public MessageAdapter(Context context, int logo, List<AdsModel> models)
    {
        super(context, logo, models);
        this.context = context;
    }

    private class ViewHolder
    {

        TextView textViewadTitle;
        TextView textViewadDesc;

    }
    public View getView(int postion, View convertView, ViewGroup parent)
    {
        ViewHolder holder = null;
        AdsModel adModel = getItem(postion);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.message_row, parent, Boolean.parseBoolean(null));
            holder = new ViewHolder();
            holder.textViewadTitle = (TextView)convertView.findViewById(R.id.agentnameTV);
            holder.textViewadDesc = (TextView) convertView.findViewById(R.id.lastloginTV);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }
        holder.textViewadTitle.setText(adModel.getName());
        holder.textViewadDesc.setText(adModel.getAdDesc());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MessagedetailsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                //context.startActivity(new Intent(context,MessageActivity.class));
            }
        });


        return convertView;
    }
}
