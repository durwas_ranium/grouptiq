package in.ranium.grouptiq;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import in.ranium.grouptiq.model.GroupBean;

/**
 * Created by durwas on 31/12/15.
 */
public class GroupDetailsActivity extends AppCompatActivity {
    ImageButton logout;
   public String headername;
    TextView actionheader;
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.groupdetais);
        actionheader=(TextView)findViewById(R.id.actionheader);
        logout=(ImageButton)findViewById(R.id.logout);
        actionheader.setText(GroupBean.getInstance().getName());
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupDetailsActivity.this, MessageActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }
}
