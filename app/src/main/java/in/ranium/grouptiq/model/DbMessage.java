package in.ranium.grouptiq.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DbMessage extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "chat";

	// Contacts table name
	private static final String TABLE_CONTACTS = "messagetable";

	// Contacts Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_GROUPID = "group_id";
	private static final String KEY_CONTENT = "content";
	private static final String KEY_CREATEDAT = "created_at";
	private static final String KEY_UPDATEDAT = "updated_at";
	private static final String TIMESTAMP = "timestamp";
	private static final String ISSEEN = "isseen";

	
	public DbMessage(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_GROUPID
				+ " TEXT, " + KEY_CONTENT + " TEXT, " + KEY_CREATEDAT
				+ " TEXT, " + KEY_UPDATEDAT + " TEXT,  " + KEY_UPDATEDAT + " TEXT, " +  TIMESTAMP + " INTEGER "+ ")";

		db.execSQL(CREATE_CONTACTS_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed

	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */

	// Adding new contact
	void addContact(Message message) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(KEY_ID, message.getId());
		values.put(KEY_GROUPID, message.getGroup_id());
		values.put(KEY_CONTENT, message.getContent());
		values.put(KEY_CREATEDAT, message.getCreated_at());
		values.put(KEY_UPDATEDAT, message.getUpdated_at());
		values.put(TIMESTAMP, message.getTimestamp());
		values.put(ISSEEN, message.getIsseen());
		db.insert(TABLE_CONTACTS, null, values);
		db.close(); // Closing database connection
	}

	// Getting All Contacts
	public List<Message> getindividualuserid(String senderid) {
		List<Message> messageList = new ArrayList<Message>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS + " WHERE "
				+ KEY_GROUPID + "=?  ORDER BY TIMESTAMP";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, new String[] { senderid,
				"individual" });

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Message message = new Message();

				message.setId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ID)));
				message.setGroup_id(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_GROUPID)));
				message.setContent(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_CONTENT)));
				message.setCreated_at(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_CREATEDAT)));
				message.setUpdated_at(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_UPDATEDAT)));
				message.setTimestamp(cursor.getString(cursor
						.getColumnIndexOrThrow(TIMESTAMP)));
				message.setIsseen(cursor.getString(cursor
						.getColumnIndexOrThrow(ISSEEN)));
				messageList.add(message);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		// return contact list
		return messageList;
	}
	
	public void deleteuser(String userid) {

		// Select All Query
		System.out.println("title--"+userid);
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_CONTACTS,
				KEY_GROUPID + "=?",
		        new String[] {userid});

		db.close();

	}
	
	public void deletegroup(String groupid) {

		// Select All Query
		System.out.println("title--"+groupid);
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_CONTACTS,
				KEY_GROUPID + "=?",
		        new String[] {groupid});

		db.close();

	}

	public String getname(String senderid) {
		String name = "";
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS + " WHERE "
				+ KEY_GROUPID + "=?";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, new String[] { senderid });

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				GroupPojo grp = new GroupPojo();

				// grp.setGroupname(cursor.getString(cursor.getColumnIndexOrThrow(KEY_GROUPNAME)));
				String fname = cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_GROUPID));
				String lname = cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_GROUPID));

				name = fname + " " + lname;
				// messageList.add(grp);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		// return contact list
		return name;
	}

	public List<Message> getallgroupmsg(String groupid) {
		List<Message> messageList = new ArrayList<Message>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS + " WHERE "
				+ KEY_GROUPID + "=?  ORDER BY TIMESTAMP";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, new String[] { groupid,
				"group" });

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Message message = new Message();

				message.setId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ID)));
				message.setGroup_id(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_GROUPID)));
				message.setContent(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_CONTENT)));
				message.setCreated_at(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_CREATEDAT)));
				message.setUpdated_at(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_UPDATEDAT)));
				message.setTimestamp(cursor.getString(cursor
						.getColumnIndexOrThrow(TIMESTAMP)));
				message.setIsseen(cursor.getString(cursor
						.getColumnIndexOrThrow(ISSEEN)));

				messageList.add(message);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		// return contact list
		return messageList;
	}

	public void updategroupseen(String groupid) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(ISSEEN, "yes");

		db.update(TABLE_CONTACTS, values, KEY_GROUPID + " = ?",
				new String[] { groupid });
	}

	public void updateindividualseen(String senderid) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ISSEEN, "yes");
		db.update(TABLE_CONTACTS, values, KEY_GROUPID + " = ?",
				new String[] { senderid });

	}

	public int getallgroupmsgcount(String groupid) {

		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS + " WHERE "
				+ KEY_GROUPID + "=? ORDER BY TIMESTAMP";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, new String[] { groupid });

		// return contact list
		return cursor.getCount();
	}

	public List<Message> getallhistory() {
		List<Message> messageList = new ArrayList<Message>();
		// Select All Query
		// String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS +
		// " GROUP BY "+GROUPID + ", "+ KEY_SENDER_USERID +
		// " ORDER BY TIMESTAMP DESC";
		// String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS +
		// " GROUP BY "
		// + GROUPID + " , " + KEY_SENDER_USERID
		// + " ORDER BY TIMESTAMP DESC";

		String selectQuery = "SELECT  * FROM messagetable WHERE chattype='individual' GROUP BY  senderuserid UNION SELECT  * FROM messagetable WHERE chattype='group' GROUP BY  groupid  ORDER BY TIMESTAMP DESC;";

		System.out.println("query---" + selectQuery);
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Message message = new Message();

				message.setId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ID)));
				message.setGroup_id(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_GROUPID)));
				message.setContent(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_CONTENT)));
				message.setCreated_at(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_CREATEDAT)));
				message.setUpdated_at(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_UPDATEDAT)));
				message.setTimestamp(cursor.getString(cursor
						.getColumnIndexOrThrow(TIMESTAMP)));
				message.setIsseen(cursor.getString(cursor
						.getColumnIndexOrThrow(ISSEEN)));
				messageList.add(message);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		// return contact list
		return messageList;
	}

	public int getunseengroupcount(String groupid, String flag) {
		int count = 0;
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS + " WHERE "
				+ KEY_GROUPID + "=? AND " + ISSEEN + "=? ";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, new String[] { groupid, flag,
				"group" });

		count = cursor.getCount();

		cursor.close();
		db.close();

		// return contact list
		return count;
	}

	public int getunseeenindividual(String senderid, String flag) {
		int count = 0;
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS + " WHERE "
				+ KEY_GROUPID + "=? AND " + ISSEEN + "=? ";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, new String[] { senderid, flag,
				"individual" });

		count = cursor.getCount();

		cursor.close();
		db.close();

		// return contact list
		return count;
	}

}