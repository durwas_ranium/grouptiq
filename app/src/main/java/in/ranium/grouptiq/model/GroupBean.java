package in.ranium.grouptiq.model;

/**
 * Created by durwas on 7/1/16.
 */
public class GroupBean {
    String name,id,archiveid;

    public String getArchiveid() {
        return archiveid;
    }

    public void setArchiveid(String archiveid) {
        this.archiveid = archiveid;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public  static GroupBean gb=new GroupBean();
    public static GroupBean getInstance()
    {
        return gb;
    }
}
