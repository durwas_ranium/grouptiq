package in.ranium.grouptiq.model;

/**
 * Created by durwas on 31/12/15.
 */
public class ActiveModel {
    public String id, name, owner_id, visibility, slug, created_at,updated_at;

    public ActiveModel(String id, String name, String owner_id, String visibility, String slug, String created_at, String updated_at) {
        this.id = id;
        this.name = name;
        this.owner_id=owner_id;
        this.visibility=visibility;
        this.slug=slug;
        this.created_at=created_at;
        this.updated_at=updated_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
    @Override
    public String toString() {
        return  id + " " + name + " "
                + slug + " " + updated_at;
    }
}
