package in.ranium.grouptiq.model;

/**
 * Created by durwas on 5/1/16.
 */


/**
 * Created by durwas on 31/12/15.
 */
public class ActiveGroupModel {
    public String id, name, owner_id, visibility, slug, created_at,updated_at,is_archived,timestamp;
    public String pivot;

    public ActiveGroupModel(String id, String name, String owner_id, String visibility, String slug, String created_at, String updated_at,String pivot,String is_archived,String timestamp) {
        this.id = id;
        this.name = name;
        this.owner_id = owner_id;
        this.visibility = visibility;
        this.slug = slug;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.pivot = pivot;
        this.is_archived = is_archived;
        this.timestamp = timestamp;

    }
    public ActiveGroupModel()
    {

    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getIs_archived() {
        return is_archived;
    }

    public void setIs_archived(String is_archived) {
        this.is_archived = is_archived;
    }

//    }

    public String getPivot() {
        return pivot;
    }

    public void setPivot(String pivot) {
        this.pivot = pivot;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
    @Override
    public String toString() {
        return  id + " " + name + " "
                + slug + " " + updated_at;
    }
}
