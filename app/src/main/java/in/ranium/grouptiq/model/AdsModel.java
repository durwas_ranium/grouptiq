package in.ranium.grouptiq.model;

/**
 * Created by Rudraksh on 02-Jul-15.
 */
public class AdsModel
{
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String name;

    public String getAdDesc() {
        return adDesc;
    }

    public void setAdDesc(String adDesc) {
        this.adDesc = adDesc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String adDesc;
    String id;

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    int logo;

    public AdsModel (String name, String desc)
    {
        this.name = name;
        this.adDesc = desc;
    }

}
