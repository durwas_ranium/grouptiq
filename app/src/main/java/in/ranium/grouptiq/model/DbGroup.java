package in.ranium.grouptiq.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DbGroup extends SQLiteOpenHelper {
 
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
    private static final String DATABASE_NAME = "groupchat";
 
    // Contacts table name
    private static final String TABLE_CONTACTS = "grouptable";
 
    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_GROUPID = "groupid";
    private static final String KEY_GROUPNAME = "groupname";
    private static final String KEY_SLUG = "slug";
    private static final String KEY_VISIBILITY = "membername";
    private static final String KEY_OWNERID = "adminid";
    private static final String CREATED_AT = "created_at";
    private static final String UPDATED_AT = "updated_at";
    private static final String ISARCHIVED = "isarchived";
    private static final String PIVOT = "pivot";
    private static final String TIMESTAMP = "timestamp";
     
    
    public DbGroup(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
 
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_GROUPID + " TEXT, " 
                + KEY_GROUPNAME + " TEXT, " +  KEY_SLUG + " TEXT, "
                +  KEY_VISIBILITY + " TEXT, " +  KEY_OWNERID + " TEXT, "
                +  CREATED_AT + " TEXT, " +  UPDATED_AT + " TEXT, "+  PIVOT + " TEXT, "+  ISARCHIVED + " TEXT, "
                +  TIMESTAMP + " INTEGER" + ")";
        
        db.execSQL(CREATE_CONTACTS_TABLE);
    }
 
    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
    	
    }
 
    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */
 
    // Adding new contact
    public void addContact(ActiveGroupModel grp) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        ContentValues values = new ContentValues();
 
        values.put(KEY_GROUPID, grp.getId());
        values.put(KEY_GROUPNAME, grp.getName());
        values.put(KEY_OWNERID, grp.getOwner_id());
        values.put(KEY_VISIBILITY, grp.getVisibility());
        values.put(KEY_SLUG, grp.getSlug());
        values.put(CREATED_AT, grp.getCreated_at());
        values.put(UPDATED_AT, grp.getUpdated_at());
        values.put(PIVOT, grp.getPivot().toString());
        values.put(ISARCHIVED, grp.getIs_archived());
        values.put(TIMESTAMP, grp.getTimestamp());
       
        
        
        db.insert(TABLE_CONTACTS, null, values);
        db.close(); // Closing database connection
    }
    
	public void updategroupname(String groupid, String groupname) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(KEY_GROUPNAME, groupname);

		db.update(TABLE_CONTACTS, values, KEY_GROUPID + " = ?",
                new String[]{groupid});
	}
 
    public void deletegroup(String groupid) {

		// Select All Query
		System.out.println("title--"+groupid);
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_CONTACTS,
                KEY_GROUPID + "=?",
                new String[]{groupid});

		db.close();

	}


    // Getting All Contacts
    public List<ActiveGroupModel> getallmembers(String groupid) {
    	List<ActiveGroupModel> messageList = new ArrayList<ActiveGroupModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS + " WHERE "+KEY_GROUPID+"=? ORDER BY TIMESTAMP";
 
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[] {groupid});
 
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ActiveGroupModel grp = new ActiveGroupModel();
                
                grp.setId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_GROUPID)));
                grp.setName(cursor.getString(cursor.getColumnIndexOrThrow(KEY_GROUPNAME)));
                grp.setOwner_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_OWNERID)));
                grp.setVisibility(cursor.getString(cursor.getColumnIndexOrThrow(KEY_VISIBILITY)));
                grp.setSlug(cursor.getString(cursor.getColumnIndexOrThrow(KEY_SLUG)));
                grp.setCreated_at(cursor.getString(cursor.getColumnIndexOrThrow(CREATED_AT)));
                grp.setUpdated_at(cursor.getString(cursor.getColumnIndexOrThrow(UPDATED_AT)));
                grp.setPivot(cursor.getString(cursor.getColumnIndexOrThrow(PIVOT)));
                grp.setIs_archived(cursor.getString(cursor.getColumnIndexOrThrow(ISARCHIVED)));
                grp.setTimestamp(cursor.getString(cursor.getColumnIndexOrThrow(TIMESTAMP)));
                messageList.add(grp);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
 
        // return contact list
        return messageList;
    }
    
    public String getgroupname(String groupid) {
    	String groupname = "";
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS + " WHERE "+KEY_GROUPID+"=? GROUP BY "+KEY_GROUPID;
 
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[] {groupid});
 
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ActiveGroupModel grp = new ActiveGroupModel();
                
//                  grp.setGroupname(cursor.getString(cursor.getColumnIndexOrThrow(KEY_GROUPNAME)));
                  groupname = cursor.getString(cursor.getColumnIndexOrThrow(KEY_GROUPNAME));
                  
                            
//                messageList.add(grp);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
 
        // return contact list
        return groupname;
    }
    
    public int checkgroup(String groupid, String memberid) {
    	int count = 0;
        // Select All Query

          String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS + " WHERE "+KEY_GROUPID+"=? AND "+KEY_OWNERID +"=?";
        System.out.println("select groupid--"+groupid);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[] {groupid, memberid});
 
        count = cursor.getCount();
      
    	cursor.close();
		db.close();

        // return contact list
        return count;
    }

    public void updategroupArchived(String groupid) {
        System.out.println("titleA--"+groupid);
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(ISARCHIVED, "yes");

        db.update(TABLE_CONTACTS, values, KEY_GROUPID + " = ?",
                new String[]{groupid});
    }
    public void updategroupJoin(String groupid) {
        System.out.println("titleJ--"+groupid);
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(ISARCHIVED, "no");

        db.update(TABLE_CONTACTS, values, KEY_GROUPID + " = ?",
                new String[]{groupid});
    }
    
    public List<ActiveGroupModel> getallhistory(String is_archived) {
    	List<ActiveGroupModel> groupList = new ArrayList<ActiveGroupModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS + " WHERE "+ISARCHIVED+"=?   ORDER BY TIMESTAMP";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[] { is_archived});
 
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ActiveGroupModel grp = new ActiveGroupModel();

                grp.setId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_GROUPID)));
                grp.setName(cursor.getString(cursor.getColumnIndexOrThrow(KEY_GROUPNAME)));
                grp.setOwner_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_OWNERID)));
                grp.setVisibility(cursor.getString(cursor.getColumnIndexOrThrow(KEY_VISIBILITY)));
                grp.setSlug(cursor.getString(cursor.getColumnIndexOrThrow(KEY_SLUG)));
                grp.setCreated_at(cursor.getString(cursor.getColumnIndexOrThrow(CREATED_AT)));
                grp.setUpdated_at(cursor.getString(cursor.getColumnIndexOrThrow(UPDATED_AT)));
                grp.setPivot(cursor.getString(cursor.getColumnIndexOrThrow(PIVOT)));
                grp.setIs_archived(cursor.getString(cursor.getColumnIndexOrThrow(ISARCHIVED)));
                grp.setTimestamp(cursor.getString(cursor.getColumnIndexOrThrow(TIMESTAMP)));
                
                
                groupList.add(grp);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
 
        // return contact list
        return groupList;
    }
 
 
}