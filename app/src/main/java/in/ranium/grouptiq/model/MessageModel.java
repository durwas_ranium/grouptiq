package in.ranium.grouptiq.model;

/**
 * Created by durwas on 7/1/16.
 */
public class MessageModel {
    String id,group_id,content,created_at,updated_at;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
    public MessageModel(String id,String group_id,String content,String created_at,String updated_at )
    {
        this.id=id;
        this.group_id=group_id;
        this.content=content;
        this.created_at=created_at;
        this.updated_at=updated_at;
    }
}
