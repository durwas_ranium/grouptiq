package in.ranium.grouptiq.model;

public class Message {

	String id, group_id, content, created_at, updated_at, timestamp, isseen;

	String groupname = "";

	public String getIsseen() {
		return isseen;
	}

	public void setIsseen(String isseen) {
		this.isseen = isseen;
	}

	public String getGroupname() {
		return groupname;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGroup_id() {
		return group_id;
	}

	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}



	public Message(String id, String group_id, String content, String created_at,
			String updated_at, String timestamp, String isseen) {
		super();
		this.id = id;
		this.group_id = group_id;
		this.content = content;
		this.created_at = created_at;
		this.updated_at = updated_at;
		this.timestamp = timestamp;
		this.isseen = isseen;
	}

	public Message() {
		super();
	}



	
}
