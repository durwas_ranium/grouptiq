package in.ranium.grouptiq.fragment;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import in.ranium.grouptiq.AddGroupActivity;
import in.ranium.grouptiq.LoginActivity;
import in.ranium.grouptiq.MessageActivity;
import in.ranium.grouptiq.R;
import in.ranium.grouptiq.model.DbGroup;
import in.ranium.grouptiq.model.GroupBean;
import in.ranium.grouptiq.model.ActiveGroupModel;
import in.ranium.grouptiq.model.RequestManger;
import in.ranium.grouptiq.model.Utils;
public class ActiveGroupFragment extends Fragment {
    View view;
    private ListView lv;
    MyCustomAdapter dataAdapter = null;
    Context context;
    ArrayList<HashMap<String, String>> productList;
    private SharedPreferences prefs;
    ArrayList<ActiveGroupModel> countryList;
    ArrayList<ActiveGroupModel> countryList2;
    DbGroup dbgroup;
    ListView listView;
    @SuppressLint("NewApi")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_one, container, false);
        context = getActivity().getApplicationContext();
        dbgroup=new DbGroup(context);
        listView = (ListView) view.findViewById(R.id.activeListView);
        countryList = new ArrayList<ActiveGroupModel>();
        countryList2 = new ArrayList<ActiveGroupModel>();
        prefs = getActivity().getSharedPreferences(RequestManger.PREFERENCES, Context.MODE_PRIVATE);
        DbGroup dbGroup= new DbGroup(getActivity());
        displayListView();
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "You want to add group  ?", Snackbar.LENGTH_LONG)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(getActivity(), AddGroupActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        }).show();
            }
        });
        registerForContextMenu(listView);
        return view;
    }
    private void displayListView() {
        new GetGroupAssyncTask().execute();
    }
    private class MyCustomAdapter extends ArrayAdapter<ActiveGroupModel> {
        private ArrayList<ActiveGroupModel> originalList;
        private ArrayList<ActiveGroupModel> countryList;
        private CountryFilter filter;
        public MyCustomAdapter(Context context, int textViewResourceId,
                               ArrayList<ActiveGroupModel> countryList) {
            super(context, textViewResourceId, countryList);
            this.countryList = new ArrayList<ActiveGroupModel>();
            this.countryList.addAll(countryList);
            this.originalList = new ArrayList<ActiveGroupModel>();
            this.originalList.addAll(countryList);
        }
        @Override
        public Filter getFilter() {
            if (filter == null) {
                filter = new CountryFilter();
            }
            return filter;
        }
        private class ViewHolder {
            TextView code;
            TextView name;
            TextView continent;
            TextView region;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            Log.v("ConvertView", String.valueOf(position));
            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.country_info, null);
                holder = new ViewHolder();
                holder.code = (TextView) convertView.findViewById(R.id.code);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.continent = (TextView) convertView.findViewById(R.id.continent);
                holder.region = (TextView) convertView.findViewById(R.id.region);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            ActiveGroupModel country = countryList.get(position);
            holder.code.setText(country.getId());
            holder.name.setText(country.getName());
            holder.continent.setText(country.getCreated_at());
            holder.region.setText(country.getSlug());
            return convertView;
        }
        private class CountryFilter extends Filter {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                Log.d("respFilter",constraint.toString());
                constraint = constraint.toString().toLowerCase();
                FilterResults result = new FilterResults();
                if (constraint != null && constraint.toString().length() > 0) {
                    ArrayList<ActiveGroupModel> filteredItems = new ArrayList<ActiveGroupModel>();
                    for (int i = 0, l = originalList.size(); i < l; i++) {
                        ActiveGroupModel country = originalList.get(i);
                        if (country.toString().toLowerCase().contains(constraint))
                            filteredItems.add(country);
                    }
                    result.count = filteredItems.size();
                    result.values = filteredItems;
                } else {
                    synchronized (this) {
                        result.values = originalList;
                        result.count = originalList.size();
                    }
                }
                return result;
            }
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                countryList = (ArrayList<ActiveGroupModel>) results.values;
                notifyDataSetChanged();
                clear();
                for (int i = 0, l = countryList.size(); i < l; i++)
                    add(countryList.get(i));
                notifyDataSetInvalidated();
            }
        }
    }
    class GetGroupAssyncTask extends AsyncTask<String, Void, String> implements RequestManger.Constantas {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            String response = "";
            try {
                Map<String, String> map = new HashMap<String, String>();
                String authkey = prefs.getString(API_KEY, "");
                String token = prefs.getString(TOKEN, "");
                map.put(RequestManger.APIKEY, "Bearer "+token);
                response = RequestManger.getHttpRequestWithHeader(map, RequestManger.HOST + "group/my?api_key="+authkey);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            Log.d("response",response);
            return response;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
            try {
                JSONObject responseJSON = new JSONObject(result);
                if (!responseJSON.has("error")) {
                    JSONArray personaArray = responseJSON.getJSONArray(DATA);
                    SaveandShow(personaArray);
                    //enables filtering for the contents of the given ListView
                    listView.setTextFilterEnabled(true);
                    EditText myFilter = (EditText) view.findViewById(R.id.inputSearch);
                    //query=myFilter.getText().toString();
                    myFilter.addTextChangedListener(new TextWatcher() {
                        public void afterTextChanged(Editable s) {
                        }
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            Log.d("respFilter", s.toString());
                            dataAdapter.getFilter().filter(s.toString());
                        }
                    });
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            // When clicked, show a toast with the TextView text
                            ActiveGroupModel country = (ActiveGroupModel) parent.getItemAtPosition(position);
                            Intent intent = new Intent(getActivity(), MessageActivity.class);
                            GroupBean.getInstance().setName(country.getName());
                            GroupBean.getInstance().setId(country.getId());
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            getActivity().startActivity(intent);
                        }
                    });
                }
                else
                {
                    String message = responseJSON.getString("error");
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                    Utils.clearPreferences(getActivity());
                    getActivity().setResult(Activity.RESULT_OK);
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                Toast.makeText(getActivity(), "Please try again.", Toast.LENGTH_LONG).show();
            }
        }
    }
    public void SaveandShow(JSONArray array) {
        try {
            Log.d("data", array.toString());
            if (array.length() > 0) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    ActiveGroupModel  model = new ActiveGroupModel();
                    Date todaydate = new Date();
                    String date = String.valueOf(todaydate.getTime());
                    model.setId(obj.getString("id"));
                    model.setName(obj.getString("name"));
                    model.setOwner_id(obj.getString("owner_id"));
                    model.setVisibility(obj.getString("visibility"));
                    model.setSlug(obj.getString("slug"));
                    model.setCreated_at(obj.getString("created_at"));
                    model.setUpdated_at(obj.getString("updated_at"));
                    model.setPivot(obj.getJSONObject("pivot").toString());
                    if(obj.getJSONObject("pivot").getString("is_archived").contains("0"))
                    {
                        model.setIs_archived("no");
                        countryList.add(model);
                        if(dbgroup.checkgroup(model.getId(), model.getOwner_id()) < 1)
                        {
                            dbgroup.addContact(new ActiveGroupModel(model.getId(), model.getName(), model.getOwner_id(), model.getVisibility(), model.getSlug(), model.getCreated_at(),model.getUpdated_at(),model.getPivot(),model.getIs_archived(),date));
                        }
                    }
                    else if (obj.getJSONObject("pivot").getString("is_archived").contains("1"))
                    {
                        model.setIs_archived("yes");
                        countryList2.add(model);
                        if(dbgroup.checkgroup(model.getId(), model.getOwner_id()) < 1)
                        {
                            dbgroup.addContact(new ActiveGroupModel(model.getId(), model.getName(), model.getOwner_id(), model.getVisibility(), model.getSlug(), model.getCreated_at(),model.getUpdated_at(),model.getPivot(),model.getIs_archived(),date));
                        }
                    }
                }
               countryList= (ArrayList<ActiveGroupModel>) dbgroup.getallhistory("no");

                dataAdapter = new MyCustomAdapter(getActivity(),
                        R.layout.country_info_two, countryList);
                dataAdapter.notifyDataSetChanged();
                // Assign adapter to ListView
                listView.setAdapter(dataAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    class ArchiveAssyncTask extends AsyncTask<String, Void, String> implements RequestManger.Constantas {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            String response = "";
            JSONObject object = new JSONObject();
            try {
                GroupBean.getInstance().setArchiveid(params[0]);
                object.put("id", params[0]);
                Map<String, String> map = new HashMap<String, String>();
                String authkey = prefs.getString(API_KEY, "");
                String token = prefs.getString(TOKEN, "");
                map.put(RequestManger.APIKEY, "Bearer "+token);
                response = RequestManger.putHttpRequestWithHeader(object,map, RequestManger.HOST + "group/toggle-archive?api_key=" + authkey);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            Log.d("response",response);
            return response;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
            try {
                JSONObject responseJSON = new JSONObject(result);
                String success=responseJSON.getString("success");
              if(success.contains("true"))
              {
                    showErrorAlert("Group Archived successfully");
                }
                else
                {
                    String message = responseJSON.getString("error");
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                    Utils.clearPreferences(getActivity());
                    getActivity().setResult(Activity.RESULT_OK);
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                Toast.makeText(getActivity(), "Please try again.", Toast.LENGTH_LONG).show();
            }
        }
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Select The Action");
        menu.add(0, v.getId(), 0, "Archive");//groupId, itemId, order, title
    }
    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final int IndexSelected=info.position;
        if(item.getTitle()=="Archive"){
            new ArchiveAssyncTask().execute( countryList.get(IndexSelected).getId());
        }
        else{
            return false;
        }
        return true;
    }
    public void showErrorAlert(final String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        TextView textMsg = new TextView(getActivity());
        textMsg.setText(message);
        textMsg.setPadding(10, 10, 10, 10);
        textMsg.setGravity(Gravity.CENTER);
        textMsg.setTextSize(18);
        builder.setView(textMsg);
        builder.setTitle("Archived Alert");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        dbgroup.updategroupArchived(GroupBean.getInstance().getArchiveid());
                        //countryList= (ArrayList<ActiveGroupModel>) dbgroup.getallhistory("no");
                        refresh();
                        Log.e("info", "OK");
                        dialog.dismiss();
                    }
                });
        builder.show();
    }
    public void refresh(){
        countryList= (ArrayList<ActiveGroupModel>) dbgroup.getallhistory("no");
        dataAdapter = new MyCustomAdapter(getActivity(),
                R.layout.country_info_two, countryList);
        dataAdapter.notifyDataSetChanged();
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);
        dataAdapter.notifyDataSetChanged();
        dataAdapter.notifyDataSetInvalidated();
        listView.invalidateViews();
    }
}
