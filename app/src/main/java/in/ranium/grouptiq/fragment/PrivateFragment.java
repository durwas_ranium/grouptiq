package in.ranium.grouptiq.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.ranium.grouptiq.R;
import in.ranium.grouptiq.model.RequestManger;

/**
 * Created by durwas on 31/12/15.
 */
public class PrivateFragment  extends Fragment {

    View view;
    ListView listViewAds;
    Context context;
    EditText email;
    Button buttonSubmit;
    private SharedPreferences prefs;
    @SuppressLint("NewApi")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_private, container, false);
        context = getActivity().getApplicationContext();

        prefs = getActivity().getSharedPreferences(RequestManger.PREFERENCES, Context.MODE_PRIVATE);
        email=(EditText)view.findViewById(R.id.email);
        buttonSubmit=(Button)view.findViewById(R.id.buttonSubmit);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String query = email.getText().toString();
                displayListView(query);
            }
        });

         return view;
    }
    private void displayListView(String email) {
        new JoinPrivategroupAssynctask().execute(email);
    }
    class JoinPrivategroupAssynctask extends AsyncTask<String, Void, String> implements RequestManger.Constantas {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            JSONObject object = new JSONObject();
            try {

                object.put("code", params[0]);
                Map<String, String> map = new HashMap<String, String>();
                String authkey = prefs.getString(API_KEY, "");
                String token = prefs.getString(TOKEN, "");
                map.put(RequestManger.APIKEY, "Bearer " + token);
                response = RequestManger.postHttpRequestWithHeader(object, map, RequestManger.HOST + "group/join-private?api_key=" + authkey);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            Log.d("response", response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }


        }

    }
}