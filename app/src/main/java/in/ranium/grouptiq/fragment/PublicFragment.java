package in.ranium.grouptiq.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.ranium.grouptiq.LoginActivity;
import in.ranium.grouptiq.R;
import in.ranium.grouptiq.model.ActiveModel;
import in.ranium.grouptiq.model.RequestManger;
import in.ranium.grouptiq.model.Utils;

/**
 * Created by durwas on 31/12/15.
 */
public class PublicFragment extends Fragment {

    View view;
    private ListView lv;
    MyCustomArchiveAdapter dataAdapter = null;
    Context context;
    ArrayList<HashMap<String, String>> productList;
    private SharedPreferences prefs;
    EditText etsearch;
    Button btnsearch;
    ArrayList<ActiveModel> countryList;

    @SuppressLint("NewApi")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_public, container, false);
        etsearch = (EditText) view.findViewById(R.id.etsearch);
        btnsearch = (Button) view.findViewById(R.id.btnsearch);
        btnsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String query = etsearch.getText().toString();
                searchData(query);
            }
        });
        context = getActivity().getApplicationContext();
        prefs = getActivity().getSharedPreferences(RequestManger.PREFERENCES, Context.MODE_PRIVATE);
        displayListView();
        return view;
    }

    private void searchData(String query) {
        new GetPublicGroupSearchAssyncTask().execute(query);
    }

    private void displayListView() {
        //Array list of countries
        countryList = new ArrayList<ActiveModel>();
        new GetPublicGroupAssyncTask().execute();
    }

    private class MyCustomArchiveAdapter extends ArrayAdapter<ActiveModel> {

        private ArrayList<ActiveModel> originalList;
        private ArrayList<ActiveModel> countryList;
        private CountryFilter filter;

        public MyCustomArchiveAdapter(Context context, int textViewResourceId,
                                      ArrayList<ActiveModel> countryList) {
            super(context, textViewResourceId, countryList);
            this.countryList = new ArrayList<ActiveModel>();
            this.countryList.addAll(countryList);
            this.originalList = new ArrayList<ActiveModel>();
            this.originalList.addAll(countryList);
        }

        @Override
        public Filter getFilter() {
            if (filter == null) {
                filter = new CountryFilter();
            }
            return filter;
        }

        @Override
        public int getCount() {
            return countryList.size();
        }

        private class ViewHolder {
            TextView code;
            TextView name;
            TextView continent;
            TextView region;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            Log.v("ConvertView", String.valueOf(position));
            if (convertView == null) {

                LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.country_info, null);

                holder = new ViewHolder();
                holder.code = (TextView) convertView.findViewById(R.id.code);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.continent = (TextView) convertView.findViewById(R.id.continent);
                holder.region = (TextView) convertView.findViewById(R.id.region);

                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ActiveModel country = countryList.get(position);
            holder.code.setText(country.getId());
            holder.name.setText(country.getName());
            holder.continent.setText(country.getCreated_at());
            holder.region.setText(country.getSlug());

            return convertView;

        }

        private class CountryFilter extends Filter {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                constraint = constraint.toString().toLowerCase();
                FilterResults result = new FilterResults();
                if (constraint != null && constraint.toString().length() > 0) {
                    ArrayList<ActiveModel> filteredItems = new ArrayList<ActiveModel>();

                    for (int i = 0, l = originalList.size(); i < l; i++) {
                        ActiveModel country = originalList.get(i);
                        if (country.toString().toLowerCase().contains(constraint))
                            filteredItems.add(country);
                    }
                    result.count = filteredItems.size();
                    result.values = filteredItems;
                } else {
                    synchronized (this) {
                        result.values = originalList;
                        result.count = originalList.size();
                    }
                }
                return result;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {

                countryList = (ArrayList<ActiveModel>) results.values;
                notifyDataSetChanged();
                clear();
                for (int i = 0, l = countryList.size(); i < l; i++)
                    add(countryList.get(i));
                notifyDataSetInvalidated();
            }
        }
    }

    class GetPublicGroupAssyncTask extends AsyncTask<String, Void, String> implements RequestManger.Constantas {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";

            try {

                Map<String, String> map = new HashMap<String, String>();
                String authkey = prefs.getString(API_KEY, "");
                String token = prefs.getString(TOKEN, "");
                map.put(RequestManger.APIKEY, "Bearer " + token);
                response = RequestManger.getHttpRequestWithHeader(map, RequestManger.HOST + "group?api_key=" + authkey);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            Log.d("response", response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }


            try {
                JSONObject responseJSON = new JSONObject(result);

                if (!responseJSON.has("error")) {
                    JSONArray personaArray = responseJSON.getJSONArray(DATA);
                    Log.d("data", personaArray.toString());
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<ActiveModel>>() {
                    }.getType();

                    countryList = gson.fromJson(personaArray.toString(), listType);
                    dataAdapter = new MyCustomArchiveAdapter(getActivity(),
                            R.layout.country_info_two, countryList);
                    ListView listView = (ListView) view.findViewById(R.id.publicListView);
                    // Assign adapter to ListView
                    listView.setAdapter(dataAdapter);

                    //enables filtering for the contents of the given ListView
                    listView.setTextFilterEnabled(true);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            // When clicked, show a toast with the TextView text
                            ActiveModel country = (ActiveModel) parent.getItemAtPosition(position);
                            showAlert(country.getId());
                        }
                    });
                } else {
                    String message = responseJSON.getString("error");
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                    Utils.clearPreferences(getActivity());
                    getActivity().setResult(Activity.RESULT_OK);
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                Toast.makeText(getActivity(), "Please try again.", Toast.LENGTH_LONG).show();
            }


        }
    }

    public void showAlert(final String countryId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure you want to join group?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        new JoinPublicgroupAssyncTask().execute(countryId);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void showErrorAlert(final String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(
                getActivity());

        TextView textMsg = new TextView(getActivity());
        textMsg.setText(message);
        textMsg.setPadding(10, 10, 10, 10);
        textMsg.setGravity(Gravity.CENTER);
        textMsg.setTextSize(18);
        builder.setView(textMsg);
        builder.setTitle("Error Alert");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        Log.e("info", "OK");
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    class GetPublicGroupSearchAssyncTask extends AsyncTask<String, Void, String> implements RequestManger.Constantas {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";

            try {
                Log.d("response", params[0]);
                Map<String, String> map = new HashMap<String, String>();
                String authkey = prefs.getString(API_KEY, "");
                String token = prefs.getString(TOKEN, "");
                map.put(RequestManger.APIKEY, "Bearer " + token);

                response = RequestManger.getHttpRequestWithHeader(map, RequestManger.HOST + "group?q=" + params[0] + "&api_key=" + authkey);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            Log.d("response", response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }


            try {
                JSONObject responseJSON = new JSONObject(result);
                JSONArray personaArray = responseJSON.getJSONArray(DATA);
                Log.d("data", personaArray.toString());
                Gson gson = new Gson();
                Type listType = new TypeToken<List<ActiveModel>>() {
                }.getType();

                countryList = gson.fromJson(personaArray.toString(), listType);
                dataAdapter = new MyCustomArchiveAdapter(getActivity(),
                        R.layout.country_info_two, countryList);
                ListView listView = (ListView) view.findViewById(R.id.publicListView);
                // Assign adapter to ListView
                listView.setAdapter(dataAdapter);

                //enables filtering for the contents of the given ListView
                listView.setTextFilterEnabled(true);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        // When clicked, show a toast with the TextView text
                        ActiveModel country = (ActiveModel) parent.getItemAtPosition(position);
                        showAlert(country.getId());

                    }
                });


            } catch (Exception ex) {
                ex.printStackTrace();
                Toast.makeText(getActivity(), "Please try again.", Toast.LENGTH_LONG).show();
            }


        }
    }

    class JoinPublicgroupAssyncTask extends AsyncTask<String, Void, String> implements RequestManger.Constantas {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";
            JSONObject object = new JSONObject();
            try {

                object.put("id", params[0]);
                Map<String, String> map = new HashMap<String, String>();
                String authkey = prefs.getString(API_KEY, "");
                String token = prefs.getString(TOKEN, "");
                map.put(RequestManger.APIKEY, "Bearer " + token);

                response = RequestManger.postHttpRequestWithHeader(object, map, RequestManger.HOST + "group/join?api_key=" + authkey);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            Log.d("responseJoin", response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        }
    }
}
