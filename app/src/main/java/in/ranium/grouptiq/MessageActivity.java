package in.ranium.grouptiq;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.ranium.grouptiq.model.GroupBean;
import in.ranium.grouptiq.model.MessageModel;
import in.ranium.grouptiq.model.RequestManger;
import in.ranium.grouptiq.model.Utils;

/**
 * Created by durwas on 31/12/15.
 */
public class MessageActivity extends AppCompatActivity{
  ;
    TextView actionheader;
    ImageButton logout;
    MyCustomMessageAdapter dataAdapter = null;
    Context context;
    private SharedPreferences prefs;
    ArrayList<MessageModel> countryList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_list);
        context = getApplicationContext();
        actionheader=(TextView)findViewById(R.id.actionheader);
        logout=(ImageButton)findViewById(R.id.back);
        actionheader.setText(GroupBean.getInstance().getName());
        prefs = getSharedPreferences(RequestManger.PREFERENCES, Context.MODE_PRIVATE);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MessageActivity.this, GroupTabActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        actionheader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MessageActivity.this, GroupDetailsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        new GetMessageAssyncTask().execute();
    }
    private class MyCustomMessageAdapter extends ArrayAdapter<MessageModel> {

        private ArrayList<MessageModel> originalList;
        private ArrayList<MessageModel> countryList;
        private CountryFilter filter;

        public MyCustomMessageAdapter(Context context, int textViewResourceId,
                                      ArrayList<MessageModel> countryList) {
            super(context, textViewResourceId, countryList);
            this.countryList = new ArrayList<MessageModel>();
            this.countryList.addAll(countryList);
            this.originalList = new ArrayList<MessageModel>();
            this.originalList.addAll(countryList);
        }

        @Override
        public Filter getFilter() {
            if (filter == null) {
                filter = new CountryFilter();
            }
            return filter;
        }

        @Override
        public int getCount() {
            return countryList.size();
        }

        private class ViewHolder {
            TextView code;
            TextView name;
            TextView continent;
            TextView region;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            Log.v("ConvertView", String.valueOf(position));
            if (convertView == null) {

                LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.country_info, null);

                holder = new ViewHolder();
                holder.code = (TextView) convertView.findViewById(R.id.code);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.continent = (TextView) convertView.findViewById(R.id.continent);
                holder.region = (TextView) convertView.findViewById(R.id.region);

                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            MessageModel country = countryList.get(position);
            holder.code.setText(country.getId());
            String data=getFirst5Words(country.getContent());
            holder.name.setText(data);
            holder.continent.setText(country.getCreated_at());
            holder.region.setText(country.getGroup_id());

            return convertView;

        }

        private class CountryFilter extends Filter {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                constraint = constraint.toString().toLowerCase();
                FilterResults result = new FilterResults();
                if (constraint != null && constraint.toString().length() > 0) {
                    ArrayList<MessageModel> filteredItems = new ArrayList<MessageModel>();

                    for (int i = 0, l = originalList.size(); i < l; i++) {
                        MessageModel country = originalList.get(i);
                        if (country.toString().toLowerCase().contains(constraint))
                            filteredItems.add(country);
                    }
                    result.count = filteredItems.size();
                    result.values = filteredItems;
                } else {
                    synchronized (this) {
                        result.values = originalList;
                        result.count = originalList.size();
                    }
                }
                return result;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {

                countryList = (ArrayList<MessageModel>) results.values;
                notifyDataSetChanged();
                clear();
                for (int i = 0, l = countryList.size(); i < l; i++)
                    add(countryList.get(i));
                notifyDataSetInvalidated();
            }
        }
    }

    class GetMessageAssyncTask extends AsyncTask<String, Void, String> implements RequestManger.Constantas {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MessageActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";

            try {

                Map<String, String> map = new HashMap<String, String>();
                String authkey = prefs.getString(API_KEY, "");
                String token = prefs.getString(TOKEN, "");
                map.put(RequestManger.APIKEY, "Bearer "+token);
                String groupId=GroupBean.getInstance().getId();
                Date todaydate = new Date();
                String date = String.valueOf(todaydate.getTime());
                response = RequestManger.getHttpRequestWithHeader(map, RequestManger.HOST + "messages?group_id="+groupId+"&time_stamp="+date+"&api_key=" + authkey);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            Log.d("responseMessage", response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
            try {
                JSONObject responseJSON = new JSONObject(result);
                if (!responseJSON.has("error")) {
                    JSONArray personaArray = responseJSON.getJSONArray(DATA);
                    Log.d("data", personaArray.toString());
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<MessageModel>>() {
                    }.getType();

                    countryList = gson.fromJson(personaArray.toString(), listType);
                    dataAdapter = new MyCustomMessageAdapter(MessageActivity.this,
                            R.layout.country_info_two, countryList);
                    ListView listView = (ListView) findViewById(R.id.messageListView);
                    // Assign adapter to ListView
                    listView.setAdapter(dataAdapter);

                    //enables filtering for the contents of the given ListView
                    listView.setTextFilterEnabled(true);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            // When clicked, show a toast with the TextView text
                            MessageModel country = (MessageModel) parent.getItemAtPosition(position);
                            Intent intent = new Intent(MessageActivity.this, MessagedetailsActivity.class);
                            intent.putExtra("message",country.getContent());
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        }
                    });

                }
                else
                {
                    String message = responseJSON.getString("error");
                    Toast.makeText(MessageActivity.this, message, Toast.LENGTH_LONG).show();
                    Utils.clearPreferences(MessageActivity.this);
                    MessageActivity.this.setResult(Activity.RESULT_OK);
                    Intent intent = new Intent(MessageActivity.this, LoginActivity.class);
                    startActivity(intent);
                    MessageActivity.this.finish();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                Toast.makeText(MessageActivity.this, "Please try again.", Toast.LENGTH_LONG).show();
            }

        }

    }
    public String getFirst5Words(String arg) {
        Pattern pattern = Pattern.compile("([\\S]+\\s*){1,5}");
        Matcher matcher = pattern.matcher(arg);
        matcher.find();
        return matcher.group();
    }
}
