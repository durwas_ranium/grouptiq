package in.ranium.grouptiq;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import in.ranium.grouptiq.fragment.ActiveGroupFragment;
import in.ranium.grouptiq.fragment.ArchiveGroupFragment;

/**
 * Created by Druv on 02-Jul-15.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter
{
    CharSequence Titles[];
    int NumbOfTabs;

    public ViewPagerAdapter(FragmentManager fragmentManager, CharSequence mTitles[], int numbOfTabs)
    {
        super(fragmentManager);
        this.Titles = mTitles;
        this.NumbOfTabs = numbOfTabs;
    }

    @Override
    public Fragment getItem(int position)
    {
        if (position == 0)
        {
            //TabBlueHits tabBlueHits = new TabBlueHits();
            return new ActiveGroupFragment();
        }
        if (position == 1)
        {
            //AllChats allChats = new AllChats();
            return new ArchiveGroupFragment();
        }

        return null;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}
