package in.ranium.grouptiq;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.ranium.grouptiq.model.RequestManger;

/**
 * Created by durwas on 1/1/16.
 */
public class LoginActivity extends AppCompatActivity {
    Button buttonLogin,buttonReg;
    EditText email,passwordEd;
    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        prefs = getSharedPreferences(RequestManger.PREFERENCES, Context.MODE_PRIVATE);
        editor = prefs.edit();
        token = prefs.getString(RequestManger.Constantas.TOKEN, null);

        if (token != null) {

            startActivity(new Intent(getApplicationContext(), GroupTabActivity.class));
            finish();
        }
        // getSupportActionBar().hide();
        buttonLogin=(Button)findViewById(R.id.buttonLogin);
        buttonReg=(Button)findViewById(R.id.buttonReg);
        email=(EditText)findViewById(R.id.email);
        passwordEd=(EditText)findViewById(R.id.password);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = email.getText().toString();
                String password = passwordEd.getText().toString();

                    if (TextUtils.isEmpty(username)) {
                        Toast.makeText(LoginActivity.this, "Please Enter Email", Toast.LENGTH_LONG).show();
                        return;
                    }
                if(isEmailValid(username)==false)
                {
                    Toast.makeText(LoginActivity.this, "Email is not Valid", Toast.LENGTH_LONG).show();
                    return;
                }

                    if (TextUtils.isEmpty(password)) {
                        Toast.makeText(LoginActivity.this, "Please Enter Password", Toast.LENGTH_LONG).show();
                        return;
                    }
                    if (RequestManger.isConnectedToInternet(LoginActivity.this)==false) {
                        Toast.makeText(LoginActivity.this, R.string.plz_chk_internet_connection, Toast.LENGTH_LONG).show();
                    } else {
                        new LoginAsynctask().execute(username, password);
                    }

            }
        });

        buttonReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression ="[a-zA-Z0-9+._%-+]{1,256}" +
                "@" +
                "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
                "(" +
                "." +
                "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" +
                ")+";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
    class LoginAsynctask extends AsyncTask<String, Void, String> implements RequestManger.Constantas {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";

            JSONObject object = new JSONObject();
            try {

                object.put("email", params[0]);
                object.put("password", params[1]);
                Map<String, String> map = new HashMap<String, String>();
//                map.put(RequestManger.APIKEY, RequestManger.APIKEYVALUE);
//                map.put(RequestManger.REQUESTERKEY, RequestManger.REQUESTERGLOBAL);
                response = RequestManger.postHttpRequestWithHeader(object, map, RequestManger.HOST + "auth/login");


            } catch (Exception ex) {
                ex.printStackTrace();
            }
            Log.d("response", response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
            try {
                JSONObject responseJSON = new JSONObject(result);

                if (!responseJSON.has("error")) {
                    String token = responseJSON.getString(TOKEN);
                    String api_key = responseJSON.getString(API_KEY);
                    String user_id = responseJSON.getString(USERID);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(TOKEN, token);
                    editor.putString(USERID, user_id);
                    editor.putString(API_KEY, api_key);
                    editor.apply();
                    Intent intent = new Intent(LoginActivity.this, GroupTabActivity.class);
                    startActivity(intent);
                    finish();

                } else {
                    JSONObject dataJSON = responseJSON.getJSONObject("error");
                    String message = dataJSON.getString(MESSAGE);
                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                Toast.makeText(LoginActivity.this, "Unable to Login", Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }
}
