package in.ranium.grouptiq;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import in.ranium.grouptiq.model.DbMessage;
import in.ranium.grouptiq.model.RequestManger;

public class GcmMessageHandler extends IntentService {
    Context context;
    //DbMessage dbmsg;
    boolean groupalert;
    String customerApiKey;
    SharedPreferences prefs;
    String userid;
    String senderid;
    String firstname;
    String chattype;
    String sender_lname;
    String logintype;
    String msgtype = "text";
    String date;
    String message;
    DbMessage dbmsg;
    public static String gcmReceived;
    SharedPreferences.Editor mainEditor;

    public GcmMessageHandler() {
        super("GcmMessageHandler");
    }

    static final String DISPLAY_MESSAGE_ACTION = "DISPLAY_MESSAGE";

    static final String EXTRA_MESSAGE = "message";
    public static final String TAG = "GCMNotificationIntentService";

    @Override
    protected void onHandleIntent(Intent intent) {

        context = getApplicationContext();
        dbmsg = new DbMessage(context);
        prefs = getSharedPreferences(RequestManger.PREFERENCES, Context.MODE_PRIVATE);
        mainEditor = prefs.edit();
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);

        if (extras != null && !extras.isEmpty()) {  // has effect of unparcelling Bundle
            // Since we're not using two way messaging, this is all we really to check for
            if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                Logger.getLogger("GCM_RECEIVED").log(Level.INFO, extras.toString());
                System.out.print("message" + message);
                Date todaydate = new Date();
                date = String.valueOf(todaydate.getTime());
                String message = intent.getExtras().getString("message");
                generateNotification(context, message, "22");
            }
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
        //}

    }

    public void getMessages() {
        new GetCustomerData().execute();
    }

    @SuppressWarnings("deprecation")
    public static void generateNotification(Context context,
                                            String message, String id) {
        System.out.print("message" + message);

        // Use NotificationCompat.Builder to set up our notification.
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

        //icon appears in device notification bar and right hand corner of notification
        builder.setSmallIcon(R.drawable.chat);

        // This intent is fired when notification is clicked
        Intent intent = new Intent(context, MessageActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        // Set the intent that will fire when the user taps the notification.
        builder.setContentIntent(pendingIntent);

        // Large icon appears on the left of the notification
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.chat));

        // Content title, which appears in large type at the top of the notification
        builder.setContentTitle("GroupTiq");

        // Content text, which appears in smaller text below the title
        builder.setContentText(message);

        // The subtext, which appears under the text on newer devices.
        // This will show-up in the devices with Android 4.2 and above only
        //builder.setSubText("Tap to view documentation about notifications.");

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        int notificationid = 0;
        String substr = id;
        // Vibrate if vibrate is enabled
        notificationid = Integer.parseInt(substr);
        notificationManager.notify(notificationid, builder.build());


    }

    class GetCustomerData extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        @SuppressWarnings("deprecation")
        protected String doInBackground(String... params) {
            String url;

            try {
                url = RequestManger.HOST + "queue_messages?customer_id=" + userid + "&persona_id=" + senderid;
                HttpClient client = new DefaultHttpClient();
                HttpGet httppost = new HttpGet(url);
                httppost.setHeader(new BasicHeader(RequestManger.APIKEY, customerApiKey));
                httppost.setHeader(new BasicHeader(RequestManger.APICONTENTTYPE, RequestManger.APICONTENTTYPEVALUE));
                //	httppost.setHeader(new BasicHeader(RequestManger.REQUESTERKEY, RequestManger.REQUESTERMOBILE));
                HttpResponse httpResponse = client.execute(httppost);
                String responseString = EntityUtils.toString(httpResponse.getEntity());
                Log.d("response", responseString);
                return responseString;
            } catch (Exception e) {
                e.printStackTrace();
                return "false";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                JSONObject array = new JSONObject(s);
                JSONArray data = array.getJSONArray("data");
                for (int i = 0; i < data.length(); i++) {
                    JSONObject obj = data.getJSONObject(i);
                    message = obj.getString("message");
                    //	dbmsg.addContact(new Message(firstname, sender_lname, message,
                    //userid, senderid, date, logintype, chattype, "no", "", "no", msgtype));
                }

                Intent intent = new Intent("CHAT_MESSAGE_RECEIVED");
                intent.putExtra("message", message);
                context.sendBroadcast(intent);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

}
