package in.ranium.grouptiq;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import in.ranium.grouptiq.fragment.PrivateFragment;
import in.ranium.grouptiq.fragment.PublicFragment;

/**
 * Created by durwas on 6/1/16.
 */
public class ViewPageradapterAdd extends FragmentStatePagerAdapter
{
    CharSequence Titles[];
    int NumbOfTabs;

    public ViewPageradapterAdd(FragmentManager fragmentManager, CharSequence mTitles[], int numbOfTabs)
    {
        super(fragmentManager);
        this.Titles = mTitles;
        this.NumbOfTabs = numbOfTabs;
    }

    @Override
    public Fragment getItem(int position)
    {
        if (position == 0)
        {
            //TabBlueHits tabBlueHits = new TabBlueHits();
            return new PublicFragment();
        }
        if (position == 1)
        {
            //AllChats allChats = new AllChats();
            return new PrivateFragment();
        }

        return null;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}
