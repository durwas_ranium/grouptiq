package in.ranium.grouptiq;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.ranium.grouptiq.model.RequestManger;

public class RegisterActivity extends AppCompatActivity {
    Button buttonLogin;
    ImageButton logout;
    EditText email;
    private SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prefs = getSharedPreferences(RequestManger.PREFERENCES, Context.MODE_PRIVATE);
       // getSupportActionBar().hide();
        buttonLogin=(Button)findViewById(R.id.buttonLogin);
        logout=(ImageButton)findViewById(R.id.logout);
        email=(EditText)findViewById(R.id.email);
                buttonLogin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String username = email.getText().toString();
                        if (TextUtils.isEmpty(username)) {
                            Toast.makeText(RegisterActivity.this, "Please Enter Email", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if(isEmailValid(username)==false)
                        {
                            Toast.makeText(RegisterActivity.this, "Email is not Valid", Toast.LENGTH_LONG).show();
                            return;
                        }


                        if (RequestManger.isConnectedToInternet(RegisterActivity.this)==false) {
                            Toast.makeText(RegisterActivity.this, R.string.plz_chk_internet_connection, Toast.LENGTH_LONG).show();
                        } else {
                            new RegAsynctask().execute(username);
//                            Intent intent = new Intent(RegisterActivity.this, GroupTabActivity.class);
//                          startActivity(intent);
                        }


                    }
                });


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

      //  String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        String expression ="[a-zA-Z0-9+._%-+]{1,256}" +
                "@" +
                "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
                "(" +
                "." +
                "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" +
                ")+";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    class RegAsynctask extends AsyncTask<String, Void, String> implements RequestManger.Constantas {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(RegisterActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String response = "";

            JSONObject object = new JSONObject();
            try {

                object.put("email", params[0]);
                Map<String, String> map = new HashMap<String, String>();
//                map.put(RequestManger.APIKEY, RequestManger.APIKEYVALUE);
//                map.put(RequestManger.REQUESTERKEY, RequestManger.REQUESTERGLOBAL);
                response = RequestManger.postHttpRequestWithHeader(object, map, RequestManger.HOST + "user");

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            Log.d("response", response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
            try {
                JSONObject responseJSON = new JSONObject(result);
                boolean error = responseJSON.getBoolean("success");

                if (error) {
                    JSONObject dataJSON = responseJSON.getJSONObject(DATA);
                    String name = dataJSON.getString(NAME);
                    String email = dataJSON.getString(EMAIL);
                    String api_key = dataJSON.getString(API_KEY);
                    String updated_at = dataJSON.getString(UPDATED_AT);
                    String created_at = dataJSON.getString(CREATED_AT);
                    String id=dataJSON.getString(ID);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(ID, id);
                    editor.putString(EMAIL, email);
                    editor.putString(NAME, name);
                    editor.putString(API_KEY, api_key);
                    editor.putString(UPDATED_AT, updated_at);
                    editor.putString(CREATED_AT, created_at);
                    editor.apply();
                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();

                } else {
                    JSONObject dataJSON = responseJSON.getJSONObject(DATA);
                    String message = dataJSON.getString(MESSAGE);
                    Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_LONG).show();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                Toast.makeText(RegisterActivity.this, "Unable to Register", Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }
}
