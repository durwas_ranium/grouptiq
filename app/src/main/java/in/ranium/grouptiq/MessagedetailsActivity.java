package in.ranium.grouptiq;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import in.ranium.grouptiq.model.GroupBean;

/**
 * Created by durwas on 31/12/15.
 */
public class MessagedetailsActivity extends AppCompatActivity {
    TextView header,details,actionheader;
    String headerdata,detailsdata;
    ImageButton logout;
   public String headername;
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_message);
        header=(TextView)findViewById(R.id.header);
        details=(TextView)findViewById(R.id.details);
        actionheader=(TextView)findViewById(R.id.actionheaderDetails);
        logout=(ImageButton)findViewById(R.id.logoutDetails);
        actionheader.setText(GroupBean.getInstance().getName());
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MessagedetailsActivity.this, MessageActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        actionheader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MessagedetailsActivity.this,GroupDetailsActivity.class);
                startActivity(intent);
            }
        });
        headerdata="Welcome to GroupTiq";
       detailsdata=getIntent().getStringExtra("message");
        header.setText(headerdata);
        details.setText(detailsdata);
    }
}
