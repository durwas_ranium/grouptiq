package in.ranium.grouptiq;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import in.ranium.grouptiq.model.RequestManger;

/**
 * Created by durwas on 30/12/15.
 */
public class VerificationActivity extends ActionBarActivity implements RequestManger.Constantas {
    Button buttonLogin;
    ImageButton logout;
    EditText vcode;
    TextView textEmail;
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verification);
        logout=(ImageButton)findViewById(R.id.logout);
        buttonLogin=(Button)findViewById(R.id.buttonLogin);
        vcode=(EditText)findViewById(R.id.vcode);
        textEmail=(TextView)findViewById(R.id.textEmail);
        textEmail.setText(getIntent().getStringExtra(EMAIL));
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = vcode.getText().toString();
                if (TextUtils.isEmpty(username)) {
                    Toast.makeText(VerificationActivity.this, "Please Enter Code", Toast.LENGTH_LONG).show();
                    return;
                }

                if (!RequestManger.isConnectedToInternet(VerificationActivity.this)) {
                    Toast.makeText(VerificationActivity.this, R.string.plz_chk_internet_connection, Toast.LENGTH_LONG).show();
                } else {
                    Intent intent=new Intent(VerificationActivity.this,GroupTabActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }


            }
        });


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VerificationActivity.this, RegisterActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        //getSupportActionBar().hide();
    }
}
